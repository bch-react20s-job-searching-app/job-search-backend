const bcrypt = require('bcrypt')
require('dotenv').config()
const LocalStrategy = require('passport-local').Strategy
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy

/**
 * @param passport
 * @param getUserByEmail
 * @param getUserById
 * Intakes passport instance,
 * function to find user by email to let authenticating able,
 * function to find user by id to deserialize the user*/
const initializePassport = (passport, findOneUser) => {
    // User authentication handler for Local Strategy
    const authenticateUser = async (email, password, done) => {
        const user = await findOneUser('email', email)
        if (!user) {
            return done(null, false, {
                message: `No user found with ${email} email!`,
            })
        } else if (user.googleId) {
            return done(null, false, {
                message: 'You have been registered with google',
            })
        }
        try {
            if (await bcrypt.compare(password, user.password)) {
                return done(null, user, { message: 'Authorized!' })
            } else {
                return done(null, false, { message: 'Wrong password' })
            }
        } catch (e) {
            return done(e)
        }
    }

    // User authentication handler for Google Strategy
    const authenticateGoogle = async (
        accessToken,
        refreshToken,
        profile,
        done
    ) => {
        console.log(profile)
        let user = await findOneUser('email', profile.emails[0].value)
        if (user) {
            return done(null, user)
        }
        let undefUser = {}
        undefUser.unsaved = true
        undefUser.googleId = profile.id
        undefUser.username = profile.displayName
        undefUser.email = profile.emails[0].value
        undefUser.image = profile.photos[0].value
        return done(null, undefUser)
    }

    // Initializing Local Strategy
    passport.use(
        new LocalStrategy({ usernameField: 'email' }, authenticateUser)
    )

    // Initializing Google Strategy
    passport.use(
        new GoogleStrategy(
            {
                clientID: process.env.GOOGLE_CLIENT_ID,
                clientSecret: process.env.GOOGLE_CLIENT_SECRET,
                callbackURL:
                    'https://silta-rest.herokuapp.com/api/auth/google/callback',
            },
            authenticateGoogle
        )
    )

    // Serializing user id or object (only in google registering case) to the session
    passport.serializeUser((user, done) => {
        if (user.unsaved === true) {
            return done(null, user)
        }
        done(null, user.id)
    })

    // Deserializing user from DB with it's Id or excluding user object from the session (in case of google registration)
    passport.deserializeUser(async (serializedData, done) => {
        if (serializedData.unsaved === true) {
            return done(null, serializedData)
        }
        let user = await findOneUser('_id', serializedData)
        // user.password = undefined
        return done(null, user)
    })
}

module.exports = { initializePassport }
