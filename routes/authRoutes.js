const router = require('express').Router()
const {
    register,
    login,
    signout,
    google,
    googleCallback,
} = require('../controllers/authHandler')

router.post('/register', register)
router.post('/login', login)
router.post('/signout', signout)
router.get('/google', google)
router.get('/google/callback', googleCallback)

module.exports = router
