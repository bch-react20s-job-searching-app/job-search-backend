const { retrieve } = require('../controllers/userHandler')
const { upload } = require('../controllers/multer.config')
const { update } = require('../controllers/userHandler')
const router = require('express').Router()

router.post('/update', upload.single('photo'), update)
router.post('/retrieve', retrieve)

module.exports = router
