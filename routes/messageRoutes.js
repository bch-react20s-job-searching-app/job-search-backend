const router = require('express').Router()
const { listAllMessages } = require('../controllers/socketHandlers.js')

router.get('/', listAllMessages)

module.exports = router
