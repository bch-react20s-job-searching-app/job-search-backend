const express = require('express')
const router = express.Router()
const authRoutes = require('./authRoutes')
const userRoutes = require('./userRoutes')
const jobRoutes = require('./jobRoutes')
const messageRoutes = require('./messageRoutes')
const matchRoutes = require('./matchRoutes')

//Setting serve to be able serve static images
router.use('/match', matchRoutes)
router.use('/auth', authRoutes)
router.use('/user', userRoutes)
router.use('/job', jobRoutes)
router.use('/images', express.static('images'))
router.use('/messages', messageRoutes)

module.exports = router
