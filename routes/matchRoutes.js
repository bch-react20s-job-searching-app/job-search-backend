const router = require('express').Router()
const {
    retrieveJobs,
    retrieveEmployees,
    likedJobs,
    likedEmployees,
    dislikedJobs,
    relevantJobs,
    relevantEmployees,
    sendJobLike,
    sendJobDislike,
    setCurrentJob,
} = require('../controllers/matchHandler')

//TODO: prefix employee / employer onto match route

// Employee match routes
router.get('/jobs', retrieveJobs)
router.get('/likedjobs/:id', likedJobs)
router.get('/dislikedjobs/:id', dislikedJobs)
router.get('/relevant-jobs/:id', relevantJobs)
router.put('/likejob/:id', sendJobLike)
router.put('/dislikejob/:id', sendJobDislike)

// Employer match routes
router.get('/users', retrieveEmployees)
router.get('/likedemployees/:id', likedEmployees)
router.get('/relevant-employees/:id', relevantEmployees)
router.put('/setcurrentjob/:id', setCurrentJob)

module.exports = router
