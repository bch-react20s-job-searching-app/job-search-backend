const {
    jobAddHandler,
    jobUpdateHandler,
    jobDeleteHandler,
} = require('../controllers/jobHandler')
const { upload } = require('../controllers/multer.config')
const router = require('express').Router()

router.post('/remove', jobDeleteHandler)
router.post('/add', upload.single('photo'), jobAddHandler)
router.post('/update', upload.single('photo'), jobUpdateHandler)

module.exports = router
