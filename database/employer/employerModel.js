let mongoose = require('mongoose')

let employerSchema = new mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    googleId: { required: false, type: String },
    linkedInId: { required: false, type: String },
    companyName: { required: false, type: String },
    hrFirstName: { required: false, type: String },
    hrTitle: { required: false, type: String },
    hrLastName: { required: false, type: String },
    hrPhoneNumber: { required: false, type: Number },
    country: { required: false, type: String },
    postalCode: { required: false, type: Number },
    headQuartersStreet: { required: false, type: String },
    headQuartersDistrict: { required: false, type: String },
    headQuartersCity: { required: false, type: String },
    description: { required: false, type: String },
    image: { required: false, type: String },
    email: { required: true, type: String },
    password: { required: false, type: String },
    username: { required: false, type: String },
    fieldOfWork: { required: false, type: String },
    primaryLanguage: { required: false, type: String },
    companyPhoneNumber: { required: false, type: Number },
    website: { required: false, type: String },
    yTunnus: { required: false, type: String },
    employeesCount: { required: false, type: Number },
    openJobs: [mongoose.Types.ObjectId],
    likedUsers: { required: false, type: [String] },
    dislikedUsers: { required: false, type: [String] },
    currentJob: { required: false, type: String },
})

module.exports = mongoose.model('Employer', employerSchema)
