let mongoose = require('mongoose')

let employeeSchema = new mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    googleId: { required: false, type: String },
    linkedInId: { required: false, type: String },
    firstname: { required: false, type: String },
    lastname: { required: false, type: String },
    DOB: { required: false, type: Date },
    location: { required: false, type: String },
    street: { required: false, type: String },
    postalCode: { required: false, type: Number },
    city: { required: false, type: String },
    country: { required: false, type: String },
    seniority: { required: false, type: String },
    skills: { required: false, type: [String] },
    description: { required: false, type: String },
    yearsOfExperience: { required: false, type: Number },
    image: { required: false, type: String },
    interests: { required: false, type: [String] },
    languages: { required: false, type: [String] },
    email: { type: String },
    phoneNumber: { type: Number, required: false },
    password: { type: String, required: false },
    username: { type: String, unique: false },
    likedJobs: { required: false, type: [String] },
    dislikedJobs: { required: false, type: [String] },
})

module.exports = mongoose.model('Employee', employeeSchema)
