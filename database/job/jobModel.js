const mongoose = require('mongoose')

let jobSchema = new mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    issuer: { required: true, type: mongoose.Types.ObjectId },
    title: { required: true, type: String },
    seniority: { required: true, type: String },
    description: { required: true, type: String },
    requiredSkills: { required: true, type: [String] },
    desirableSkills: { required: true, type: [String] },
    location: { required: true, type: [String] },
    possibilityRemote: { required: true, type: Boolean },
    image: { required: false, type: String },
    workingLanguage: { required: true, type: String },
    companyWebsite: { required: true, type: String },
    contractType: { required: true, type: [String] },
    // liked and dislike are part of job model since the employer is only deciding yes/no for specific job
    likedUsers: { required: false, type: [String] },
    dislikedUsers: { required: false, type: [String] },
    //Needed for showing the user what company has published this jpb
    companyName: { required: true, type: String },
})

module.exports = mongoose.model('Job', jobSchema)
