const mongoose = require('mongoose')
const Employer = require('./employer/employerModel')
const Employee = require('./employee/employeeModel')
const Job = require('./job/jobModel')
const {
    createFilterObj,
    employerDocToObject,
    employeeDocToObject,
} = require('../controllers/dbHelper')
const bcrypt = require('bcrypt')
const Validator = require('../validator/validator')

//A placeholder for the variable
let database

/**
 * Connects to the Mongo DataBase and adds listeners for success connection and errors
 * @returns connection instance**/
const connectToMongo = () => {
    //If the database variable already set, then it will not produce any new connections
    if (database) {
        return
    }

    //Connecting to Mongo
    mongoose
        .connect(process.env.DB_CONNECTION_LINK, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true,
        })
        .catch((error) => console.log(error))

    //Setting connection instance to the database variable
    database = mongoose.connection

    //Success listener
    database.once('open', async () => console.log('Connected'))

    //Failure listener
    database.on('error', (err) => {
        console.log(err)
    })

    return database
}

/**
 * @param filterObj intakes the object with filter key and the value entrie
 * Searching only through the Employees collection
 * @returns either found document, or null*/
const findOneEmployee = async (filterObj) => {
    return Employee.findOne(filterObj, (err, data) => {
        if (err) {
            return err.message
        }
        return data
    })
}

/**
 * @param filterObj intakes the object with filter key and the value entrie
 * Searching only through the Employers collection
 * @returns either found document, or null*/
const findOneEmployer = async (filterObj) => {
    return Employer.findOne(filterObj, (err, data) => {
        if (err) {
            return err.message
        }
        return data
    })
}

/**
 *
 * @param {string} filter intakes the string by which field of document to do searches
 * @param {string} value intakes the string of value to compare with the documents
 * @param {boolean} add type of the user of not, by default not
 * @returns user document or null
 */
const findOneUser = async (filter, value, collectionReturn = false) => {
    return new Promise(async (resolve) => {
        const filterObj = createFilterObj(filter, value)
        if (typeof filterObj === 'object') {
            let res = await findOneEmployer(filterObj)
            if (res) {
                collectionReturn && (res.type = 'employer')
                return resolve(res)
            } else {
                res = await findOneEmployee(filterObj)
                if (res) {
                    collectionReturn && (res.type = 'employee')
                    return resolve(res)
                }
                return resolve(null)
            }
        } else {
            return resolve(null)
        }
    })
}

/**
 *
 * @param {string} filter intakes the string by which field of document to do searches
 * @param {string} value intakes the string of value to compare with the documents
 * @returns user document or null
 */
const findOneJob = async (filter, value) => {
    return new Promise((resolve) => {
        const filterObj = createFilterObj(filter, value)
        Job.findOne(filterObj, (err, doc) => {
            if (err) {
                console.log(err)
                return resolve(null)
            }
            resolve(doc)
        })
    })
}

/**
 * @param {object} employerObj
 * @returns user created -> returns user document
 * @returns already user with this email -> returns string with message
 */
const signUpEmployee = async (employeeObj) => {
    return new Promise(async (resolve, reject) => {
        if (await findOneUser('email', employeeObj.email)) {
            return resolve(
                `There are already an account assigned to ${employeeObj.email}`
            )
        }
        Employee.create(
            {
                _id: new mongoose.Types.ObjectId(),
                googleId: employeeObj.googleId || '',
                linkedInId: employeeObj.linkedInId || '',
                firstname: employeeObj.firstname || '',
                lastname: employeeObj.lastname || '',
                age: employeeObj.age || 0,
                DOB: employeeObj.DOB || new Date(),
                location: employeeObj.location || '',
                street: employeeObj.street || '',
                postalCode: employeeObj.postalCode || 0,
                city: employeeObj.city || '',
                country: employeeObj.country || '',
                seniority: employeeObj.seniority || '',
                skills: employeeObj.skills || [''],
                description: employeeObj.description || '',
                yearsOfExperience: employeeObj.yearsOfExperience || 0,
                image: employeeObj.image || '',
                interests: employeeObj.interests || [''],
                languages: employeeObj.languages || [''],
                phoneNumber: employeeObj.phoneNumber || 0,
                email: employeeObj.email,
                password: employeeObj.googleId
                    ? ''
                    : await bcrypt.hash(employeeObj.password, 10),
                username: employeeObj.username,
            },
            (err, doc) => {
                if (err) {
                    return reject(err.message)
                }
                // const userObj = employeeDocToObject(doc)
                return resolve(doc)
            }
        )
    })
}

/**
 * @param {object} employerObj
 * @returns user created -> returns user document
 * @returns already user with this email -> returns string with message
 */
const signUpEmployer = async (employerObj) => {
    return new Promise(async (resolve, reject) => {
        if (await findOneUser('email', employerObj.email)) {
            return resolve(
                `There are already an account assigned to ${employerObj.email}`
            )
        }
        Employer.create(
            {
                _id: new mongoose.Types.ObjectId(),
                googleId: employerObj.googleId || '',
                linkedInId: employerObj.linkedInId || '',
                companyName: employerObj.username || '',
                hrFirstName: employerObj.hrFirstName || '',
                hrTitle: employerObj.hrTitle || '',
                hrLastName: employerObj.hrLastName || '',
                hrPhoneNumber: employerObj.hrPhoneNumber || 0,
                country: employerObj.country || '',
                postalCode: employerObj.postalCode || 0,
                headQuartersStreet: employerObj.headQuartersStreet || '',
                headQuartersDistrict: employerObj.headQuartersDistrict || '',
                headQuartersCity: employerObj.headQuartersCity || '',
                description: employerObj.description || '',
                image: employerObj.image || '',
                email: employerObj.email || '',
                password: employerObj.googleId
                    ? ''
                    : await bcrypt.hash(employerObj.password, 10),
                username: employerObj.username || '',
                fieldOfWork: employerObj.fieldOfWork || '',
                primaryLanguage: employerObj.primaryLanguage || '',
                companyPhoneNumber: employerObj.companyPhoneNumber,
                website: employerObj.website || '',
                yTunnus: employerObj.yTunnus || '00000',
                employeesCount: employerObj.employeesCount || 0,
                currentJob: employerObj.currentJob,
                openJobs: [],
            },
            (err, doc) => {
                if (err) {
                    return reject(err.message)
                }
                // const userObj = employerDocToObject(doc)
                return resolve(doc)
            }
        )
    })
}

/**
 *
 * @param {string} user - mongoDB id
 * @param {object} updates
 * @returns updated user object || null if there is a problem
 */
const updateUser = async (user, updates) => {
    return new Promise(async (resolve, reject) => {
        let userToUpdate = await findOneUser('_id', user, true)
        switch (userToUpdate.type) {
            case 'employer': {
                let updatesObj = Validator.updateEmployer(updates)
                if (updatesObj.password)
                    updatesObj.password = await bcrypt.hash(
                        updatesObj.password,
                        10
                    )
                Object.assign(userToUpdate, updatesObj)
                await userToUpdate.save((err, doc) => {
                    if (err) {
                        return reject(err.message)
                    }
                    const userObj = employerDocToObject(doc)
                    return resolve(userObj)
                })
                break
            }
            case 'employee': {
                console.log(updates)
                let updatesObj = Validator.updateEmployee(updates)
                console.log(updatesObj)
                if (updatesObj.password)
                    updatesObj.password = await bcrypt.hash(
                        updatesObj.password,
                        10
                    )
                Object.assign(userToUpdate, updatesObj)
                await userToUpdate.save((err, doc) => {
                    if (err) {
                        return reject(err.message)
                    }
                    const userObj = employeeDocToObject(doc)
                    return resolve(userObj)
                })
                break
            }
            default:
                return
        }
    })
}

/**
 *
 * @param {object} jobObject - validated object to create a new job
 * @returns {err || doc} - document if job is created, error message if something went wrong
 */
const addJob = async (jobObject) => {
    return new Promise(async (resolve) => {
        await Job.create(
            {
                _id: new mongoose.Types.ObjectId(),
                issuer: jobObject.issuer,
                title: jobObject.title,
                seniority: jobObject.seniority,
                description: jobObject.description,
                requiredSkills: jobObject.requiredSkills,
                desirableSkills: jobObject.desirableSkills,
                location: jobObject.location,
                possibilityRemote: jobObject.possibilityRemote,
                image: jobObject.image,
                workingLanguage: jobObject.workingLanguage,
                companyWebsite: jobObject.companyWebsite,
                contractType: jobObject.contractType,
                companyName: jobObject.companyName,
            },
            (err, doc) => {
                if (err) {
                    return resolve(err.message)
                }
                return resolve(doc)
            }
        )
    })
}

/**
 *
 * @param {string} userId - mongoDB id
 * @param {string} jobId - mongoDB id
 * @param {object} updates
 * @returns updated job object || null if there is a problem
 */
const updateJob = async (userId, jobId, updates) => {
    return new Promise(async (resolve) => {
        let jobToUpdate = await findOneJob('_id', jobId)
        if (!jobToUpdate) return resolve(null)
        if (String(jobToUpdate.issuer) !== String(userId)) {
            return resolve(null)
        }
        const updatesObj = Validator.updateJob(updates)
        Object.assign(jobToUpdate, updatesObj)
        jobToUpdate.save((err, doc) => {
            if (err) {
                return resolve(err.message)
            }
            return resolve(doc)
        })
    })
}

/**
 *
 * @param {string} userId
 * @param {string} jobId
 * @returns string if something went wrong
 * @returns true if document is deleted
 */
const deleteJob = async (userId, jobId) => {
    return new Promise(async (resolve) => {
        const user = await findOneUser('_id', userId)
        if (!user) return resolve('no user find with this id')

        if (!user.openJobs || user.openJobs.indexOf(jobId) === -1)
            return resolve('you have not posted this job...')

        const filterObj = createFilterObj('_id', jobId)

        Job.deleteOne(filterObj, (err) => {
            if (err) {
                return resolve(err.message)
            }
            return resolve(true)
        })
    })
}

module.exports = {
    database,
    connectToMongo,
    findOneUser,
    signUpEmployee,
    signUpEmployer,
    updateUser,
    addJob,
    updateJob,
    deleteJob,
}
