# Development process guidelines
## General
- Project timeline 24.3.2021 - 21.5.2021
- Sprints
    - 24.3. - 26.3. Set-up & Planning
    - 31.3. - 9.4. - ???
    - 14.4. - 23.4. - ???
    - 28.4. - 7.4. - (MVP items ready?)
    - 12.4. - 20.4. MVP+ items?
    - 21.5. final demo for Telia
- Primary teamwork schedule
    - Wednesdays
        - Previous sprint demo 09:30 - 09:45
        - Previous sprint retrospective 09:45 - 10:15
        - Sprint planning 10:15 - 11:00
        - Joint development time 11:45 - 15:00
    - Thu - Fri
        - Daily meetup 09:30 - 10:00
    - Rest of the week, 'voluntary coding' :) ie. if timetables allow
  
## Git branching strategy:
- master
    - "Production" branch
    - Commits **must** only be merges from "staging" branch
- staging
    - Code ready to be merged to master
    - Commits **must** satisfy MVP and be **squashed** merges from "dev" branch
- **dev**
    - **Main development** branch from which all development should branch out from
    - Commits **must** be merges from feature/bug branches via pull requests
- feature/*
    - Feature branches
    - Commits **must** include commit msg with prefix `feature/%feature name%: `
- bugfix/*
    - Bug fixing branches for existing features
    - Commits **must** include commit msg with prefix `bugfix/%feature name%: `

### Step-by-step example for starting on a new feature:
Make sure you're code is up-to-date, and then **branch out from dev**:
```
$ git pull --all
$ git checkout dev
$ git checkout -b feature/%my_awesome_new_feature%
```
Once done, or *at least* once a day when you've been coding, add & commit:
```
$ git add %files_that_you_added%
$ git commit -m "feature/%my_awesome_new_feature%: your commit msg here"
```

And push to remote. 1st time with:
```
$ git push --set-upstream origin feature/%my_awesome_new_feature%
```
Subsequent times go directly with:
```
$ git push
```
Finally, once you're done with coding and testing your feature, and have pushed them to remote, create a pull request on GitLab to begin merge of your feature branch back to the **dev branch**.  
See link for more detailed instructions:  
https://docs.gitlab.com/ee/user/project/merge_requests/