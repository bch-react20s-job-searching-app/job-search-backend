const mongoose = require('mongoose')

/** 
 *Methods of this class provide validation functions
 @returns instance of class*/
class Validator {
    /**
     * @param email string
     * @returns True if email has passed the validation and False if not*/
    email(email) {
        return /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/.test(
            email
        )
    }

    /**
     * @param username string
     * @returns True if username is not falsy*/
    username(username) {
        return !!username
    }

    /**
     * @param objectId string
     * @returns True if objectId is instance of mongoose.Types.ObjectId*/
    objectId(objectId) {
        // return objectId instanceof mongoose.Types.ObjectId
        return mongoose.Types.ObjectId.isValid(objectId)
    }

    /**
     * @param password password string
     * @returns True if password's length more then 8*/
    password(password) {
        return password && password.length >= 8
    }
    /**
     * @param string
     * @returns True if string is not falsy and
     * False if string is actually falsy*/
    checkEmptyString(string) {
        return !!string
    }

    /**
     *
     * @param {string} string
     * @returns false if type of the arg passed is not string and true if it is string
     */
    checkStrictString(string) {
        return typeof string === 'string'
    }

    /**
     * may be used for checking arrays and objects
     * @param {object} object || array
     * @returns false if type of the arg passed is not object and true if it is object
     */
    checkStrictObject(object) {
        return typeof object === 'object'
    }

    /**
     * may be used for checking arrays and objects
     * @param {object} object || array
     * @returns false if type of the arg passed is not object and true if it is object
     */
    checkStrictBoolean(boolean) {
        return typeof boolean === 'boolean'
    }

    /**
     * @param signUpObj object with data to sign up employee
     * @returns True if object has passed the validation and False if have not passed*/
    signUpEmployee(signUpObj) {
        return !!(
            this.email(signUpObj.email) &&
            (signUpObj.googleId ? true : this.password(signUpObj.password))
        )
    }

    /**
     * @param signInObj object with data to sign in
     * @returns True if object has passed the validation and False if have not passed*/
    signIn(signInObj) {
        return !!(
            this.email(signInObj.email) && this.password(signInObj.password)
        )
    }

    /**
     * @param signUpObj object with data to sign up employer
     * @returns True if object has passed the validation and False if have not passed*/
    signUpEmployer(signUpObj) {
        return !!(
            this.email(signUpObj.email) &&
            (signUpObj.googleId ? true : this.password(signUpObj.password))
        )
    }

    /**
     *
     * @param {object} updates
     * @returns object ready to put it into update function as an object of updates
     */
    updateEmployer(updates) {
        let checked = {}

        if (typeof updates.companyName === 'string')
            checked.companyName = updates.companyName
        if (typeof updates.hrFirstName === 'string')
            checked.hrFirstName = updates.hrFirstName
        if (typeof updates.hrTitle === 'string')
            checked.hrTitle = updates.hrTitle
        if (typeof updates.hrLastName === 'string')
            checked.hrLastName = updates.hrLastName
        if (typeof updates.hrPhoneNumber === 'string')
            checked.hrPhoneNumber = +updates.hrPhoneNumber
        if (typeof updates.country === 'string')
            checked.country = updates.country
        if (typeof updates.postalCode === 'string')
            checked.postalCode = +updates.postalCode
        if (typeof updates.headQuartersStreet === 'string')
            checked.headQuartersStreet = updates.headQuartersStreet
        if (typeof updates.headQuartersDistrict === 'string')
            checked.headQuartersDistrict = updates.headQuartersDistrict
        if (typeof updates.headQuartersCity === 'string')
            checked.headQuartersCity = updates.headQuartersCity
        if (typeof updates.description === 'string')
            checked.description = updates.description
        if (typeof updates.image === 'string') checked.image = updates.image
        if (typeof updates.password === 'string')
            checked.password = updates.password
        if (typeof updates.username === 'string')
            checked.username = updates.username
        if (typeof updates.fieldOfWork === 'string')
            checked.fieldOfWork = updates.fieldOfWork
        if (typeof updates.primaryLanguage === 'string')
            checked.primaryLanguage = updates.primaryLanguage
        if (typeof updates.companyPhoneNumber === 'string')
            checked.companyPhoneNumber = +updates.companyPhoneNumber
        if (typeof updates.website === 'string')
            checked.website = updates.website
        if (typeof updates.yTunnus === 'string')
            checked.yTunnus = updates.yTunnus
        if (typeof updates.employeesCount === 'number')
            checked.employeesCount = updates.employeesCount
        if (typeof updates.openJobs === 'object')
            checked.openJobs = updates.openJobs

        return checked
    }

    /**
     *
     * @param {object} updates
     * object ready to put it into update function as an object of updates
     */
    updateEmployee(updates) {
        let checked = {}
        if (typeof updates.firstname === 'string')
            checked.firstname = updates.firstname
        if (typeof updates.lastname === 'string')
            checked.lastname = updates.lastname
        if (typeof updates.DOB === 'string') checked.DOB = updates.DOB
        if (typeof updates.location === 'string')
            checked.location = updates.location
        if (typeof updates.street === 'string') checked.street = updates.street
        if (typeof updates.postalCode === 'number')
            checked.postalCode = updates.postalCode
        if (typeof updates.password === 'string')
            checked.password = updates.password
        if (typeof updates.username === 'string')
            checked.username = updates.username
        if (typeof updates.city === 'string') checked.city = updates.city
        if (typeof updates.country === 'string')
            checked.country = updates.country
        if (typeof updates.seniority === 'number')
            checked.seniority = updates.seniority
        if (typeof updates.skills === 'string')
            checked.skills = updates.skills.toString().split(',')
        if (typeof updates.description === 'string')
            checked.description = updates.description
        if (typeof updates.yearsOfExperience === 'number')
            checked.yearsOfExperience = updates.yearsOfExperience
        if (typeof updates.image === 'string') checked.image = updates.image
        if (typeof updates.interests === 'string')
            checked.interests = updates.interests.toString().split(',')
        if (typeof updates.languages === 'string')
            checked.languages = updates.languages.toString().split(',')
        if (typeof updates.phonenumber === 'string')
            checked.phoneNumber = +updates.phonenumber

        return checked
    }

    /**
     * @param {jobObj} object with data to sign up employee
     * @returns True if object has passed the validation and False if have not passed*/
    createJob(jobObj) {
        // return !!(
        //     this.checkEmptyString(jobObj.issuer) &&
        //     this.checkStrictString(jobObj.title) &&
        //     this.checkStrictString(jobObj.seniority) &&
        //     this.checkStrictString(jobObj.description) &&
        //     this.checkStrictString(jobObj.workingLanguage) &&
        //     this.checkStrictString(jobObj.companyWebsite) &&
        //     this.checkStrictObject(jobObj.requiredSkills) &&
        //     this.checkStrictObject(jobObj.desirableSkills) &&
        //     this.checkStrictObject(jobObj.location) &&
        //     this.checkStrictObject(jobObj.contractType) &&
        //     // this.checkStrictBoolean(jobObj.possibilityRemote) &&
        //     this.checkStrictString(jobObj.companyName))

        let checked = []

        if (typeof jobObj.title === 'string') checked.title = jobObj.title
        checked.issuer = jobObj.issuer
        checked.companyName = jobObj.companyName

        if (typeof jobObj.seniority === 'string')
            checked.seniority = jobObj.seniority

        if (typeof jobObj.description === 'string')
            checked.description = jobObj.description

        if (typeof jobObj.image === 'string') checked.image = jobObj.image

        if (typeof jobObj.workingLanguage === 'string')
            checked.workingLanguage = jobObj.workingLanguage

        if (typeof jobObj.companyWebsite === 'string')
            checked.companyWebsite = jobObj.companyWebsite

        if (typeof jobObj.requiredSkills === 'string')
            checked.requiredSkills = jobObj.requiredSkills.toString().split(',')

        if (typeof jobObj.desirableSkills === 'string')
            checked.desirableSkills = jobObj.desirableSkills
                .toString()
                .split(',')

        if (typeof jobObj.location === 'string')
            checked.location = jobObj.location.toString().split(',')

        if (typeof jobObj.contractType === 'string')
            checked.contractType = jobObj.contractType.toString().split(',')

        if (typeof jobObj.possibilityRemote === 'string')
            //bool
            checked.possibilityRemote = jobObj.possibilityRemote

        return checked
    }

    /**
     *
     * @param {object} updates
     * object ready to put it into update function as an object of updates
     */
    updateJob(updates) {
        let checked = []

        if (typeof updates.title === 'string') checked.title = updates.title

        if (typeof updates.seniority === 'string')
            checked.seniority = updates.seniority

        if (typeof updates.description === 'string')
            checked.description = updates.description

        if (typeof updates.image === 'string') checked.image = updates.image

        if (typeof updates.workingLanguage === 'string')
            checked.workingLanguage = updates.workingLanguage

        if (typeof updates.companyWebsite === 'string')
            checked.companyWebsite = updates.companyWebsite

        if (typeof updates.requiredSkills === 'object')
            checked.requiredSkills = updates.requiredSkills

        if (typeof updates.desirableSkills === 'object')
            checked.desirableSkills = updates.desirableSkills

        if (typeof updates.location === 'object')
            checked.location = updates.location

        if (typeof updates.contractType === 'object')
            checked.contractType = updates.contractType

        if (typeof updates.possibilityRemote === 'boolean')
            checked.possibilityRemote = updates.possibilityRemote

        return checked
    }
}

module.exports = new Validator()
