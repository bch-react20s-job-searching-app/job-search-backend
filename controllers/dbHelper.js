const Validator = require('../validator/validator')

/**
 * @param filter intakes the string by which field of document to do searches
 * @param value intakes the string of value to compare with the documents
 * @param baseObj by default - {} - the basis for the returned object
 * @returns  filterObj */
const createFilterObj = (filter, value, baseObj = {}) => {
    if (
        !Validator.checkEmptyString(filter) ||
        !Validator.checkEmptyString(value)
    ) {
        return console.log('wrong arguments')
    }
    let filterObj = baseObj
    filterObj[filter] = value
    return filterObj
}

/**
 *
 * @param {Document} doc - employer document
 * @returns object with deleted password and _v fields, also deletes Document properties
 */
const employerDocToObject = (doc) => ({
    _id: doc._id,
    companyName: doc.companyName,
    hrFirstName: doc.hrFirstName,
    hrTitle: doc.hrTitle,
    hrLastName: doc.hrLastName,
    hrPhoneNumber: doc.hrPhoneNumber,
    country: doc.country,
    postalCode: doc.postalCode,
    headQuartersStreet: doc.headQuartersStreet,
    headQuartersDistrict: doc.headQuartersDistrict,
    headQuartersCity: doc.headQuartersCity,
    description: doc.description,
    image: doc.image,
    email: doc.email,
    username: doc.username,
    fieldOfWork: doc.fieldOfWork,
    primaryLanguage: doc.primaryLanguage,
    companyPhoneNumber: doc.companyPhoneNumber,
    website: doc.website,
    yTunnus: doc.yTunnus,
    employeesCount: doc.employeesCount,
    openJobs: doc.openJobs,
})

/**
 *
 * @param {Document} doc - employee document
 * @returns object with deleted password and _v fields, also deletes Document properties
 */
const employeeDocToObject = (doc) => ({
    _id: doc._id,
    firstname: doc.firstname,
    lastname: doc.lastname,
    age: doc.age,
    DOB: doc.DOB,
    location: doc.location,
    street: doc.street,
    postalCode: doc.postalCode,
    city: doc.city,
    country: doc.country,
    seniority: doc.seniority,
    skills: doc.skills,
    description: doc.description,
    yearsOfExperience: doc.yearsOfExperience,
    image: doc.image,
    interests: doc.interests,
    languages: doc.languages,
    phoneNumber: doc.phoneNumber,
    email: doc.email,
    username: doc.username,
})

module.exports = { createFilterObj, employerDocToObject, employeeDocToObject }
