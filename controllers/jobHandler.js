const {
    addJob,
    updateJob,
    updateUser,
    deleteJob,
} = require('../database/database')
const Validator = require('../validator/validator')
const cloudinary = require('./cloudinary.config')

/**
 * @param {Request} req
 * @param {Response} res
 * @returns {Response} either an object with greeting and made document or and object with error message
 */
const jobAddHandler = async (req, res) => {
    if (!req.user || typeof req.user.yTunnus !== 'string') {
        return res.json({
            message: 'you have to be logged in as company',
        })
    }

    req.body.issuer = req.user._id
    req.body.companyName = req.user.companyName

    console.log(req.body)

    //If there is a file in request, then we firstly upload it into the cloud, and after it add the link to the file as the image property of the req.body
    if (req.file) {
        const result = await cloudinary.uploader.upload(req.file.path)
        req.body.image = result?.secure_url || ''
    }

    addJob(Validator.createJob(req.body))
        .then((r) => {
            if (typeof r !== 'string') {
                updateUser(req.user._id, {
                    openJobs: [...req.user.openJobs, r.id],
                })
                return res.json({
                    message: 'Job created!',
                    job: r,
                })
            } else {
                res.json({ message: r })
            }
        })
        .catch((e) => {
            console.log(e)
            return res.json({ message: 'internal problemes and memes' })
        })
}

/**
 * @param {Request} req
 * @param {Response} res
 * @returns {Response} either an object with greeting and made document or and object with error message
 */
const jobUpdateHandler = async (req, res) => {
    if (!req.body) {
        return res.json({
            err: 'you need to send updates object to actually update job',
        })
    }
    if (!req.user)
        return res.json({
            err: 'you have to be logged in to change your jobs',
        })

    if (!Validator.objectId(req.body.jobId))
        return res.json({ err: 'you have submitted wrong type of jobId' })

    //If there is a file in request, then we firstly upload it into the cloud, and after it add the link to the file as the image property of the req.body
    if (req.file) {
        const result = await cloudinary.uploader.upload(req.file.path)
        req.body.image = result?.secure_url || ''
    }

    updateJob(req.user._id, req.body.jobId, req.body)
        .then((r) => {
            if (r === null) {
                return res.json({
                    err:
                        'something went wrong, please re-check data you have submitted',
                })
            }
            return res.json(r)
        })
        .catch((e) => {
            console.log(e)
            return res.json({ err: 'internal error' })
        })
}

/**
 *
 * @param {Request} req
 * @param {Response} res
 * @returns either a string with error or greeting if the document has been deleted
 */
const jobDeleteHandler = (req, res) => {
    if (!req.body) {
        return res.json({
            err: 'you need to send jobId to actually delete job',
        })
    }
    if (!req.user)
        return res.json({
            err: 'you have to be logged in to delete job',
        })

    if (!Validator.objectId(req.body.jobId)) {
        return res.json({ err: 'you have submitted wrong type of jobId' })
    }
    deleteJob(req.user._id, req.body.jobId)
        .then((r) => {
            if (typeof r === 'string') {
                return res.json({ err: r })
            }
            updateUser(req.user._id, {
                openJobs: [
                    ...req.user.openJobs.filter(
                        (item) => String(item) !== req.body.jobId
                    ),
                ],
            })
            return res.json({ deleted: true })
        })
        .catch((e) => {
            console.log(e)
            return res.json({ err: 'internal error' })
        })
}

module.exports = { jobAddHandler, jobUpdateHandler, jobDeleteHandler }
