const Validator = require('../validator/validator')
const {
    signUpEmployee,
    signUpEmployer,
    findOneUser,
} = require('../database/database')
const { initializePassport } = require('../passport/passport.config')
const passport = require('passport')

initializePassport(passport, findOneUser)

/**
 *
 * @param {Request} req
 * @param {Response} res
 * @returns Response with JSON object containing user object <- if everything went successfull
 * @returns Response with JSON object containing message <- if wrong data submitted
 * @returns Response with JSON object containing error <- if internal error has occured
 */
const register = (req, res) => {
    if (!req.body) {
        res.json({ err: 'No data provided' })
    }
    switch (req.body.type) {
        case 'employer':
            if (req.body.googleId) {
                if (!Validator.email(req.body.email)) {
                    return res.json({ message: 'Wrong data submitted' })
                }
            } else if (!Validator.signUpEmployer(req.body)) {
                return res.json({ message: 'Wrong data submitted' })
            }
            signUpEmployer(req.body)
                .then((r) => {
                    if (typeof r !== 'string') {
                        // return res.json({
                        //     message: 'Employer created!',
                        //     user: r,
                        // })
                        req.login(r, function (err) {
                            if (err) {
                                return res.json({
                                    message: 'internal error',
                                })
                            }
                            r.password = undefined
                            return res.json({
                                message: 'authenticated',
                                user: r,
                            })
                        })
                    } else {
                        res.json({ message: r })
                    }
                })
                .catch((e) => {
                    console.log(e)
                    return res.json({ message: 'internal error' })
                })
            break
        case 'employee':
            if (req.body.googleId) {
                if (!Validator.email(req.body.email)) {
                    return res.json({ message: 'Wrong data submitted' })
                }
            } else if (!Validator.signUpEmployee(req.body)) {
                return res.json({ message: 'Wrong data submitted' })
            }
            signUpEmployee(req.body)
                .then((r) => {
                    if (typeof r !== 'string') {
                        // return res.json({
                        //     message: 'Employee created!',
                        //     user: r,
                        // })
                        req.login(r, function (err) {
                            if (err) {
                                return res.json({
                                    message: 'internal error',
                                })
                            }
                            r.password = undefined
                            return res.json({
                                message: 'authenticated',
                                user: r,
                            })
                        })
                    } else {
                        res.json({ message: r })
                    }
                })
                .catch((e) => {
                    console.log(e)
                    return res.json({ message: 'internal error' })
                })
            break
        default:
            return res.json({
                message: 'You have not mentioned type of user to create',
            })
    }
}

/**
 * @param {Request} req
 * @param {Response} res
 * @returns Response with JSON object containing user object
 */
const login = (req, res) => {
    passport.authenticate('local', function (err, user) {
        if (err) {
            return res.json({ message: err.message })
        }
        if (!user) {
            return res.json({
                message: 'incorrect credentials',
            })
        }
        req.login(user, function (err) {
            if (err) {
                return res.json({ message: 'internal error' })
            }
            user.password = undefined
            return res.json({
                message: 'authenticated',
                user: user,
            })
        })
    })(req, res)
}

/**
 * @param {Request} req
 * @param {Response} res
 * @returns Perfoms log out process and then deletes client side session cookie
 */
const signout = (req, res) => {
    req.logout()
    req.session.destroy(function (err) {
        if (!err) {
            res.status(200)
                .clearCookie('connect.sid', { path: '/' })
                .json({ user: req.user })
        } else {
            console.log(err)
        }
    })
}

const google = passport.authenticate('google', {
    scope: ['profile', 'email'],
})

const googleCallback = (req, res) => {
    passport.authenticate('google', function (err, user) {
        if (err) {
            return res.json({ message: err.message })
        }
        if (!user) {
            return res.json({ message: 'user not found' })
        }
        req.login(user, function (err) {
            if (err) {
                return res.json({ message: 'internal error' })
            }
            // return res.json({
            //     message: 'authenticated',
            //     user: user,
            // })
            return res.redirect('https://silta.netlify.app/')
        })
    })(req, res)
}

module.exports = {
    register,
    login,
    signout,
    google,
    googleCallback,
}
