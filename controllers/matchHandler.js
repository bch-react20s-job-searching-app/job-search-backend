const mongoose = require('mongoose')
const Job = require('../database/job/jobModel')
const Employee = require('../database/employee/employeeModel')
const Employer = require('../database/employer/employerModel')

// Connect to mongoDB

// Retrieve list of jobs from the server -- ensure that jobs / employees that the user have already seen are not shown again

///
///
///// EMPLOYEE METHODS
///

// retirves all the jobs from the DB
const retrieveJobs = (req, res) => {
    Job.find({}, (err, jobs) => {
        if (err) {
            res.status(500).send(err)
        }
        res.status(200).send(jobs)
    })
}

// Retrieves the jobs liked by a specific user found by _id
// TODO: ensure that this functionality can be provided on cleint side instead of just in postman
const likedJobs = (req, res) => {
    const id = req.params.id
    Employee.findOne({ _id: id }, { likedJobs: 1 }, function (err, likedJobs) {
        if (err) {
            res.status(500).send(err)
        }
        res.status(200).json(likedJobs)
    })
}

const relevantJobs = (req, res) => {
    const id = req.params.id
    // Finds employee with ID which is parsed from the request, returns liked and disliked jobs
    Employee.findOne(
        { _id: id },
        { likedJobs: 1, dislikedJobs: 1 },
        function (err, returnedJobs) {
            if (err) {
                res.status(500).send(err)
            }
            Job.find({}, (err, allJobs) => {
                // concats liked and disliked Jobs ID to allow filtering
                let likedAndDisliked = returnedJobs.likedJobs.concat(
                    returnedJobs.dislikedJobs
                )

                let filteredJobs = []

                // filters Jobs and populates the filteredJobs array with all properties of the likedAndDisliked jobs
                allJobs.filter(function (newData) {
                    return likedAndDisliked.filter(function (oldData) {
                        if (newData._id.toString() === oldData) {
                            filteredJobs.push(newData)
                        }
                    })
                })
                // final filter, compares the list of likedJobs and All jobs and returns those that don't match
                const filterJobs = (first, second) => {
                    const spreaded = [...first, ...second]
                    return spreaded.filter((el) => {
                        return !(first.includes(el) && second.includes(el))
                    })
                }

                let usersJobs = filterJobs(allJobs, filteredJobs)

                res.status(200).send(usersJobs)
            })
        }
    )
}
// Retrieves the jobs disliked by a specific user found by _id
// TODO: ensure that this functionality can be provided on cleint side instead of just in postman
const dislikedJobs = (req, res) => {
    const id = req.params.id
    Employee.findOne(
        { _id: id },
        { dislikedJobs: 1 },
        function (err, dislikedJobs) {
            if (err) {
                res.status(500).send(err)
            }
            res.status(200).json(dislikedJobs)
        }
    )
}

const sendJobLike = (req, res) => {
    const id = req.params.id
    Employee.findOneAndUpdate(
        { _id: id },
        { $push: { likedJobs: req.body.job } },
        function (error, likedJobs) {
            if (error) {
                res.status(200).send(error)
            } else {
                res.status(200).send(likedJobs)
            }
        }
    )
}

const sendJobDislike = (req, res) => {
    const id = req.params.id
    Employee.findOneAndUpdate(
        { _id: id },
        { $push: { dislikedJobs: req.body.job } },
        function (error, dislikedJobs) {
            if (error) {
                res.status(200).send(error)
            } else {
                res.status(200).send(dislikedJobs)
            }
        }
    )
}

// grabs the job to add to liked list from the request body
/* 
    Employee.findOne({ _id: id }, { likedJobs: 1 }, function (err, likedJobs) {
        if (err) {
            res.status(500).send(err)
        }

        console.log(likedJobs)
        // likedJobs.push(addLike)
        res.status(200).json(likedJobs)
        */

////
////// EMPLOYER ROUTES
/////

const retrieveEmployees = (req, res) => {
    Employee.find({}, (err, employees) => {
        if (err) {
            res.status(500).send(err)
        }
        res.status(200).send(employees)
    })
}

// Users liked by employers for certain job
const likedEmployees = (req, res) => {
    const id = req.params.id
    Job.find({ _id: id }, { likedUsers: 1 }, function (err, likedUsers) {
        if (err) {
            res.status(500).send(err)
        }
        res.status(200).json(likedUsers)
    })
}

//TODO: be consistant with using employee or user
const relevantEmployees = (req, res) => {
    const id = req.params.id
    // Finds Employer with ID which is parsed from the request, returns liked and disliked jobs
    Employer.findOne({ _id: id }, {}, function (err, returnedUsers) {
        if (err) {
            res.status(500).send(err)
        }
        Employee.find({}, (err, allUsers) => {
            // concats liked and disliked Jobs ID to allow filtering
            let likedAndDisliked = returnedUsers.likedUsers.concat(
                returnedUsers.dislikedUsers
            )

            let filteredUsers = []

            console.log(returnedUsers)

            // filters Jobs and populates the filteredJobs array with all properties of the likedAndDisliked jobs
            allUsers.filter(function (newData) {
                return likedAndDisliked.filter(function (oldData) {
                    if (newData._id.toString() === oldData) {
                        filteredUsers.push(newData)
                    }
                })
            })

            console.log(filteredUsers)
            // final filter, compares the list of likedJobs and All jobs and returns those that don't match
            const filterUsers = (first, second) => {
                const spreaded = [...first, ...second]
                return spreaded.filter((el) => {
                    return !(first.includes(el) && second.includes(el))
                })
            }

            let relevantUsers = filterUsers(allUsers, filteredUsers)

            res.status(200).send(relevantUsers)
        })
    })
}

const sendEmployeeLike = (req, res) => {
    const id = req.params.id

    Employee.findOneAndUpdate(
        { _id: id },
        { $push: { likedJobs: req.body.job } },
        function (error, likedJobs) {
            if (error) {
                res.status(200).send(error)
            } else {
                res.status(200).send(likedJobs)
            }
        }
    )
}

// required so that
const setCurrentJob = (req, res) => {
    const id = req.params.id
    console.log(req.body.currentJob)
    Employer.findOneAndUpdate(
        { _id: id },
        { $set: { currentJob: req.body.currentJob } },
        { new: true },
        function (error, currentJob) {
            if (error) {
                res.status(200).send(error)
            } else {
                res.status(200).send(currentJob)
            }
        }
    )
}

//TODO: ensure that the job itself is pulled from the params
const sendEmployeeDislike = (req, res) => {
    const id = req.params.id
    Job.findOneAndUpdate(
        { _id: id },
        { $push: { dislikedJobs: req.body.job } },
        function (error, dislikedJobs) {
            if (error) {
                res.status(200).send(error)
            } else {
                res.status(200).send(dislikedJobs)
            }
        }
    )
}

// select yes or no to employee -- will need to handle that selection and mark the job / person as liked or not,

// GIGLALIKE implementation

module.exports = {
    retrieveJobs,
    likedEmployees,
    likedJobs,
    dislikedJobs,
    relevantJobs,
    retrieveEmployees,
    relevantEmployees,
    sendJobLike,
    sendJobDislike,
    setCurrentJob,
}
