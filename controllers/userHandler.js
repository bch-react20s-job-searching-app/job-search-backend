const { updateUser } = require('../database/database')
const cloudinary = require('./cloudinary.config')

/**
 *
 * @param {Request} req
 * @param {Response} res
 * @returns updated user object
 */
const update = async (req, res) => {
    if (!req.body) return res.json({ err: 'no data submitted' })

    if (!req.user)
        return res.json({
            err: 'you have to be logged in to change your profile',
        })

    //If there is a file in request, then we firstly upload it into the cloud, and after it add the link to the file as the image property of the req.body
    if (req.file) {
        const result = await cloudinary.uploader.upload(req.file.path)
        req.body.image = result?.secure_url || ''
    }

    updateUser(req.user._id, req.body)
        .then((r) => {
            if (typeof r !== 'string') {
                return res.json({
                    message: 'User have been updated successfully',
                    user: r,
                })
            }
            return res.json({ message: r })
        })
        .catch((e) => {
            console.log(e)
            return res.json({ err: 'internal error' })
        })
}

/**
 * @param {Request} req
 * @param {Response} res
 * @returns Response with JSON object containing user object
 */
const retrieve = (req, res) => {
    let user = req.user
    return res.json(user)
}

module.exports = { update, retrieve }
