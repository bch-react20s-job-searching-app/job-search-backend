// listAllMessages function - To list all messages

const Message = require('../database/message/Message')
const listAllMessages = (req, res) => {
    Message.find({}, (err, messages) => {
        if (err) {
            res.status(500).send(err)
        }
        res.status(200).json(messages)
    })
}

module.exports = {
    listAllMessages,
}
