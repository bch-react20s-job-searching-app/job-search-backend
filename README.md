# Backend for Telia project

## Contents:

-   [Eslint installation](#eslint)
-   [Authentication API](#auth)
-   [Jobs(_Steve_) API](#jobs)

<h2 id="eslint">Eslint installation</h2>

-   installing eslint globally

```
npm i -g eslint
```

-   install eslint extension to VSCode(using VSCode extension market)
-   PROFITTT1111!!!
-   for this project eslint config is already configured

---

<h2 id="auth">Authentication API</h2>

### POSTing to '/api/auth/login'

-   How To Post? Just send an object of email and password

-   if credentials okay\* -> returns object that contains user object without password
-   if credentials not okay -> returns object with key message with the entrie of string 'user not found'
-   if nothing submitted -> returns object with key message with the entrie of string 'wrong data submitted'

### POSTing to '/api/auth/signup'

-   How To Post? Send an object with the key of type and there are two possible types: 'employee' || 'employer'. The other part of the object contains user object with some different fields, like username, email, password and etc\*1

-   if everything is okay, then returned the user object without password
-   if nothing submitted || data not passes the validation -> returns object with key message with the entrie of string 'wrong data submitted'
-   if email mentioned in the form is already assigned to another account ->

### POSTing to '/api/auth/signout'

-   How To Post? Just post nothing to the route and you will receive the result...

-   by default returns status 200, deletes the cookie on the user side and deletes the session drom DB

\*user signed = cookie is set to the browser and session is added to the DB

\*user not signed = three cases to be so:

problem with saving cookie

-   session is not saved to the DB

-   user is just not signed in :D

### Opening an 'api/auth/google'

-   opening this page redirects you to google services to authentication

-   after that returns you to '/api/auth/google/callback'

-   and this route after serializing the user will redirect him to '/'

### POSTing to '/api/user/retrieve'

-   How To Post? Just post nothing to the route and you will receive the result...

-   if user signed\* -> returns user object
-   if user is not signed -> returns empty object

### POSTing to '/api/user/update

-   _important_ if you sending image to update route, don't forget to:

    -   add name="photo" and id="photo" to your file input field
    -   add enctype="multipart/form-data" to your form tag

-   You just have to be logged in and send an object with updates and the function will return a new version of user
-   If user is not logged in, server will response with an object with error string

---

## The examples of Post requests and the results:

### Login

Request:

```json
{
    "email": "test.testilainen@testaila.com",
    "password": "1234567890"
}
```

Response if authentication succeded:

```json
{
    "message": "authenticated",
    "user": {
        "openJobs": [],
        "_id": "606dd663b39eed6123b24913",
        "companyName": "a",
        "image": "",
        "email": "test.testilainen@testaila.com",
        "password": "dont hack me pls",
        "username": "some_su",
        "__v": 0
    }
}
```

Response if authentication failed:

```json
{
    "message": "user not found"
}
```

Response if no data has been submited:

```json
{
    "message": "Wrong data submitted"
}
```

### SignUp

Request signing up an employer:

```json
{
    "type": "employer",
    "companyName": "a", //Optional
    "email": "test.testilainen.@testail1.com", //Required
    "password": "1234567890", //Required (at least 8 symbols)
    "username": "testy", //Required
    "hrFirstName": "test string", //Optional
    "hrTitle": "test string", //Optional
    "hrLastName": "test string", //Optional
    "hrPhoneNumber": 1, //Optional
    "country": "test string", //Optional
    "postalCode": 1, //Optional
    "headQuartersStreet": "test string", //Optional
    "headQuartersDistrict": "test string", //Optional
    "headQuartersCity": "test string", //Optional
    "description": "test string", //Optional
    "image": "test string", //Optional
    "fieldOfWork": "test string", //Optional
    "primaryLanguage": "test string", //Optional
    "companyPhoneNumber": 1, //Optional
    "website": "test string", //Optional
    "yTunnus": "test string", //Optional
    "employeesCount": 1, //Optional
    "openJobs": []
}
```

Request signing up an employee:

```json
{
    "type": "employee", //Defining the type of the user and the collection where to save him
    "email": "qweqwe.wqe2we@gmail.com", //Required
    "password": "123456789", //Required (at least 8 symbols)
    "username": "zhopa", //Optional
    "city": "moscow", //Optional
    "country": "russia", //Optional
    "skills": [""], //Optional
    "interests": [""], //Optional
    "languages": [""], //Optional
    "firstname": "", //Optional
    "lastname": "", //Optional
    "age": 0, //Optional
    "DOB": Date, //Optional
    "location": "", //Optional
    "street": "", //Optional
    "postalCode": 0, //Optional
    "city": "", //Optional
    "country": "", //Optional
    "seniority": "", //Optional
    "description": "", //Optional
    "yearsOfExperience": 0, //Optional
    "image": "", //Optional
    "phoneNumber": 0 //Optional
}
```

Repsonse with successful added employer:

```json
{
    "message": "Employer created!",
    "user": {
        "openJobs": [],
        "_id": "606dc6e2c432615ee6be3906",
        "companyName": "a",
        "hrFirstName": "test string",
        "hrTitle": "test string",
        "hrLastName": "test string",
        "hrPhoneNumber": 1,
        "country": "test string",
        "postalCode": 1,
        "headQuartersStreet": "test string",
        "headQuartersDistrict": "test string",
        "headQuartersCity": "test string",
        "description": "test string",
        "image": "",
        "email": "test.testilainen.@testail1.com",
        "password": "dont hack me pls",
        "username": "testy",
        "fieldOfWork": "test string",
        "primaryLanguage": "test string",
        "companyPhoneNumber": 1,
        "website": "test string",
        "yTunnus": "test string",
        "employeesCount": 1,
        "__v": 0
    }
}
```

Response with successful added employee:

```json
{
    "message": "Employee created!",
    "user": {
        "skills": [""],
        "interests": [""],
        "languages": [""],
        "_id": "606d7cccbd46b15d167b7c95",
        "firstname": "",
        "lastname": "",
        "age": 0,
        "DOB": "2021-04-07T09:35:08.256Z",
        "location": "",
        "street": "",
        "postalCode": 0,
        "city": "moscow",
        "country": "russia",
        "seniority": "",
        "description": "",
        "yearsOfExperience": 0,
        "image": "",
        "phoneNumber": 0,
        "email": "qweqwe.wqe2we@gmail1.com",
        "password": "dont hack me pls",
        "username": "zhopa",
        "__v": 0
    }
}
```

Response if there is already the user with this email:

```json
{
    "err": "There are already an account assigned to qweqwe.wqe2we@gmail.com"
}
```

Response if the data submitted to the server was wrong:

```json
{
    "message": "Wrong data submitted"
}
```

### Retrieve

No request body is needed

Response if user is signed in:

```json
{}
```

Response if user is not signed in:

```json
{
    "openJobs": [],
    "_id": "606dd663b39eed6123b24913",
    "companyName": "a",
    "image": "",
    "email": "test.testilainen@testaila.com",
    "password": "dont hack me pls",
    "username": "some_su",
    "__v": 0
}
```

### SignOut

No request body needed

Response:

nothing, not even an empty object or null, just nothing

### Update

Request body:

```json
{
    "username": "12345"
}
```

Response if everything went well:

```json
{
    "_id": "6081e4f930acfb220c1ac162",
    "firstname": "",
    "lastname": "",
    "DOB": "2021-04-22T21:04:57.518Z",
    "location": "",
    "street": "",
    "postalCode": 0,
    "city": "",
    "country": "",
    "seniority": "",
    "skills": [""],
    "description": "",
    "yearsOfExperience": 0,
    "image": "",
    "interests": [""],
    "languages": [""],
    "phoneNumber": 0,
    "email": "test.test@test1.com",
    "username": "12345"
}
```

Response if user is not loggen in:

```json
{
    "err": "you have to be logged in to change your profile"
}
```

<h2 id="jobs">Jobs API</h2>

### POSTing to 'api/job/add

-   You need to be logged in

-   You need to have a company account to post to this route

-   You may post a photo to this route to create, but you have to use very easy rules for this:

    -   add name="photo" and id="photo" to your file input field
    -   add enctype="multipart/form-data" attribute to your form tag

-   The person whose account was used to create this job gets an update of his openJobs array with the id of this job, same as the job's issuer field is an Id of the user who has made this job

### POSTing to 'api/job/update

-   You need to be logged in

-   You need to have a company account to post to this route

-   You may post a photo to this route to create, but you have to use very easy rules for this:

    -   add name="photo" and id="photo" to your file input field
    -   add enctype="multipart/form-data" attribute to your form tag

### POSTing to 'api/job/delete

-   You need to be logged in

-   You need to have a company account to post to this route

---

## The examples of Post requests and the results:

### Adding

Example of Request body:

```json
{
    "title": "test", //Required
    "seniority": "test", //Required
    "description": "test", //Required
    "requiredSkills": ["test"], //Required
    "desirableSkills": ["test"], //Required
    "location": ["test"], //Required
    "possibilityRemote": true, //Required
    "workingLanguage": "test", //Required
    "companyWebsite": "test", //Required
    "contractType": ["test"] //Required
}
```

Response to this Request:

```json
{
    "message": "Job created!",
    "job": {
        "requiredSkills": ["test"],
        "desirableSkills": ["test"],
        "location": ["test"],
        "contractType": ["test"],
        "_id": "608c657dacdd7246f751068f",
        "issuer": "6081e4ef30acfb220c1ac161",
        "title": "test",
        "seniority": "test",
        "description": "test",
        "possibilityRemote": true,
        "workingLanguage": "test",
        "companyWebsite": "test",
        "__v": 0
    }
}
```

Example of Response, when you not signed in as a company account:

```json
{
    "err": "you have to be logged in as company"
}
```

Example of BAD Request body:

```json
{
    "title": "test", //Required
    "seniority": "test", //Required
    "description": "test", //Required
    "requiredSkills": ["test"], //Required
    "desirableSkills": ["test"], //Required
    "location": ["test"], //Required
    "possibilityRemote": true, //Required
    "workingLanguage": "test", //Required
    "companyWebsite": "test" //Required
    //"contractType": ["test"] //this field is missed
}
```

Response to this Request:

```json
{
    "message": "wrong data submitted"
}
```

### Updating

Example of Request body:

```json
{
    "title": "updated title",
    "seniority": "30 seconds better then was before"
}
```

\*plus file submitted

Response to this Request:

```json
{
    "requiredSkills": ["test"],
    "desirableSkills": ["test"],
    "location": ["test"],
    "contractType": ["test"],
    "_id": "608c657dacdd7246f751068f",
    "issuer": "6081e4ef30acfb220c1ac161",
    "title": "updated title",
    "seniority": "30 seconds better then was before",
    "description": "test",
    "possibilityRemote": true,
    "workingLanguage": "test",
    "companyWebsite": "test",
    "__v": 0,
    "image": "images/2021-04-30T20:31:41.134Z-iu.jpeg"
}
```

Example of Response, when you not signed in as a company account:

```json
{
    "err": "you have to be logged in to change your jobs"
}
```

Response to the empty Request:

```json
{
    "message": "you need to send updates object to actually update job"
}
```

Response if you are sending wrong jobId:

```json
{
    "message": "you have submitted wrong type of jobId"
}
```

Response if something internal and ethereal has happened:

```json
{
    "message": "something went wrong, please re-check data you have submitted"
}
```

OR

```json
{
    "message": "internal error"
}
```

### Deleteing

Example of Request body:

```json
{
    "jobId": "608c657dacdd7246f751068f"
}
```

Response to this Request:

```json
{
    "deleted": true
}
```

Example of Response, when you not signed in as a company account:

```json
{
    "err": "you have to be logged in to delete job"
}
```

Response to the empty Request:

```json
{
    "message": "you need to send jobId to actually delete job"
}
```

Response if you are sending wrong jobId:

```json
{
    "message": "you have submitted wrong type of jobId"
}
```

Response if something internal and ethereal has happened:

```json
{
    "message": "internal error"
}
```
