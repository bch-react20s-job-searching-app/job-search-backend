const { connectToMongo } = require('./database/database')
const MongoClient = require('mongodb').MongoClient
const express = require('express')
const session = require('express-session')
const passport = require('passport')
const MongoStore = require('connect-mongo')(session)
const cors = require('cors')
require('dotenv').config()
const PORT = process.env.PORT || 8080
const HOST = process.env.HOST || '127.0.0.1'
const http = require('http')
const routes = require('./routes/routesMain')
const { Server } = require('socket.io')
const path = require('path')
const NEW_CHAT_MESSAGE_EVENT = 'newChatMessage'
const Message = require('./database/message/Message')

const app = express()

//DB connect establishment
let db = connectToMongo()

// allows socket io to use the server to connect
const server = http.createServer(app)

//Bodyparser Middleware
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
// app.use(cors({ credentials: true, origin: process.env.ALLOWED_ORIGINS })) // to ensure requests made on the frontend are processed on the backend for now add localhost:3000 to .env as ALLOWED_ORIGINS
app.use(cors())

//Initializing Express session
app.use(
    session({
        secret: process.env.SESSION_SECRET,
        resave: false,
        saveUninitialized: false,
        store: new MongoStore({
            mongooseConnection: db,
            collection: 'sessions',
        }),
        cookie: { maxAge: 60 * 60 * 1000 * 24 * 30 },
    })
)

//Applying Passport middleware
app.use(passport.initialize())
app.use(passport.session())

//Adding information about other routes to app
app.use('/api', routes)

//This part of code is serving react app from build folder if that one exists
app.use(express.static(path.join(__dirname, 'build')))

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'build/index.html'))
})

//Starting server
server.listen(PORT, HOST, () => {
    console.log(`The server is listening on ${PORT}`)
})

// socket IO
const io = new Server(server, {
    cors: {
        origins: process.env.ALLOWED_ORIGINS || 3000,
    },
})

io.on('connection', async (socket) => {
    // Join a conversation
    const { roomId } = socket.handshake.query
    socket.join(roomId)

    // Listen for new messages
    socket.on(NEW_CHAT_MESSAGE_EVENT, (data) => {
        io.in(roomId).emit(NEW_CHAT_MESSAGE_EVENT, data)
    })

    // save messsages to DB

    socket.on(NEW_CHAT_MESSAGE_EVENT, (message) => {
        let chatMessage = new Message({
            message: message.body,
            sender: 'Anonymous',
        })
        chatMessage.save()
    })

    // Leave the room if the user closes the socket
    socket.on('disconnect', () => {
        socket.leave(roomId)
    })
})

server.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`)
})
